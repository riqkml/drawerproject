import React from "react";
import { StyleSheet, Text, View } from "react-native";

const Section = ({ name }) => {
  return (
    <View>
      <Text>{name}</Text>
    </View>
  );
};

export default Section;

const styles = StyleSheet.create({});
