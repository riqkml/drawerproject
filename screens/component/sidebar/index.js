import {
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from "@react-navigation/drawer";
import React from "react";
import {
  Button,
  Dimensions,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/FontAwesome";
import { Profile } from "../../images";
import Section from "../section";
const height = Dimensions.get("screen").height;
const width = Dimensions.get("screen").width;

const Sidebar = (props) => {
  const navigation = props.navigation;
  const routes = props.state.routeNames;
  const selectedRoute = props.state.routeNames[props.state.index];
  console.log(selectedRoute);
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
      <View style={{ flex: 1 }}>
        <LinearGradient
          start={{ x: 0, y: 3 }}
          end={{ x: 1, y: 3 }}
          colors={["#00d4ff", "#2DABE0", "#0299d8", "#34AEE2"]}
          style={{
            paddingBottom: height * 0.1,
            paddingTop: height * 0.08,
            zIndex: 2,
            height: height * 0.3,
          }}
        >
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <View>
              <View
                style={{
                  width: width * 0.29,
                  height: height * 0.15,
                  borderRadius: width * 0.29,
                  borderWidth: 2,
                  borderColor: "white",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Image
                  source={Profile}
                  style={{
                    width: width * 0.28,
                    height: height * 0.14,
                    borderRadius: width * 0.28,
                  }}
                />
              </View>
              <View
                style={{
                  width: 20,
                  height: 20,
                  backgroundColor: "#4BCA1B",
                  borderRadius: 20,
                  position: "absolute",
                  bottom: 6,
                  right: 6,
                  borderColor: "white",
                  borderWidth: 2,
                }}
              />
            </View>
            <View
              style={{
                marginTop: 15,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: "700",
                  color: "white",
                }}
              >
                Riqki Kamal Amrela
              </Text>
              <Text style={{ fontSize: 14, fontWeight: "300", color: "white" }}>
                Student SALT
              </Text>
            </View>
          </View>
        </LinearGradient>
        <DrawerContentScrollView {...props}>
          <DrawerItemList {...props} />
          <DrawerItem
            label="Setting"
            icon={({ focused, color, size }) => (
              <Icon color={color} size={size} name="gears" />
            )}
          />
          <View
            style={{ height: 40, borderColor: "grey", borderBottomWidth: 0.5 }}
          />
          <View style={{ height: 10 }} />
          <DrawerItem
            label="Log Out"
            icon={({ focused, color, size }) => (
              <Icon color={color} size={size} name="sign-out" />
            )}
          />
        </DrawerContentScrollView>
      </View>
    </SafeAreaView>
  );
};

export default Sidebar;

const styles = StyleSheet.create({});
