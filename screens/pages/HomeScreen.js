import React, { Component } from "react";
import { Button, Text, View } from "react-native";

export default class HomeScreen extends Component {
  render() {
    return (
      <View>
        <Text> Home Screen</Text>
        <Button title="klik disini" color="#841584" />
      </View>
    );
  }
}
