import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import Router from "./router";
import { createDrawerNavigator } from "@react-navigation/drawer";
import HomeScreen from "./pages/HomeScreen";
import MessageScreen from "./pages/MessageScreen";
import Sidebar from "./component/sidebar";
const Drawer = createDrawerNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Router />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
