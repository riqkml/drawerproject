import { StackActions } from "@react-navigation/native";
import React, { Component } from "react";
import { Text, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "../pages/HomeScreen";
import DetailScreen from "../pages/DetailScreen";
import { createDrawerNavigator } from "@react-navigation/drawer";
import MessageScreen from "../pages/MessageScreen";
import Sidebar from "../component/sidebar";
import Icon from "react-native-vector-icons/FontAwesome";
import DocumentScreen from "../pages/DocumentScreen";
const Stack = createStackNavigator();
const hide = { headerShown: false };
const Drawer = createDrawerNavigator();
// drawerContent={(props) => <Sidebar {...props} />
const DrawerTab = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Home"
      drawerContent={(props) => (
        <Sidebar
          {...props}
          // navigation={props.navigation}
          // routes={props.state.routeNames}
          // selectedRoute={props.state.routeNames[props.state.index]}
        />
      )}
      drawerContentOptions={{
        activeTintColor: "#2DABE0",
        itemStyle: { marginVertical: 5 },
      }}
    >
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: "Home",
          drawerIcon: ({ focused, color, size }) => (
            <Icon color={color} size={size} name="home" />
          ),
        }}
      />
      <Drawer.Screen
        name="Message"
        component={MessageScreen}
        options={{
          title: "Message",
          drawerIcon: ({ focused, color, size }) => (
            <Icon color={color} size={size} name="comments" />
          ),
        }}
      />
      <Drawer.Screen
        name="Document"
        component={DocumentScreen}
        options={{
          title: "Document",
          drawerIcon: ({ focused, color, size }) => (
            <Icon color={color} size={size} name="folder" />
          ),
        }}
      />
    </Drawer.Navigator>
  );
};
export default class Router extends Component {
  render() {
    return (
      <Stack.Navigator>
        <Stack.Screen name="Home" component={DrawerTab} options={hide} />
        <Stack.Screen name="Detail" component={DetailScreen} options={hide} />
      </Stack.Navigator>
    );
  }
}
